from analyzer import ModInfo, loadProc, saveProc
from pathlib import Path
import subprocess as sp
import os, glob, shutil, time
import traceback

zipdir = Path(__file__).parent / 'zips'
extdir = Path(__file__).parent / 'ext'
instdir = Path(__file__).parent / 'instal'

def extractDir(zipname):
    print(f'Extracting [{zipname}]')
    devnull = open(os.devnull, 'w')
    sp.call(['C:/Program Files/7-Zip/7z', 'x', f'-o{extdir / zipname}', zipdir / zipname ], stdout=devnull, stderr=devnull)
    return extdir / zipname

def findDirInDir(dirname, topdir, fail=False):
  for dir, subdirs, _ in os.walk(topdir):
    if dirname.upper() in [n.upper() for n in subdirs]:
        return dir
  if fail:
    raise BaseException(f'{dirname} does not exist in {topdir}')
  else:
    return None

def findFileInDir(filename, topdir):
  for dir, _, files in os.walk(topdir):
    if filename.upper() in [n.upper() for n in files]:
        return dir
  raise BaseException(f'{filename} does not exist in {topdir}')

def findExpectedFiles(modinfo:ModInfo):
  for expectedfile in modinfo.expectedFiles:
    print(f'searching for [{expectedfile}]')
    loc = findFileInDir(expectedfile, extdir)

def rmdir(dirpath:Path):
  if dirpath.exists():
    print(f'Removing [{dirpath}]')
    command = ['rmdir', '/S', '/Q', dirpath.absolute()]
    try:
      proc = sp.check_output(command, shell=True, stderr=sp.STDOUT)
    except sp.CalledProcessError:
      print('There was an error - command exited error output')

def findStrategy(mi:ModInfo):
  commands = []
  validate = False
  if len(mi.extractPath) == 1:
    realdestdir = str(instdir / mi.extractPath[0].replace('/home/username/games/', ''))
    # extract all zips in order to the single path
    for n, zfn in enumerate(mi.zipFilename):
      commands.append({'copyover' if n > 0 else 'copy': str(extdir / zfn), 'to': realdestdir})
  if len(mi.extractPath) > 1:
    # check each subfolder name and see if there is a zip subdir that matches it
    for destdir in mi.extractPath:
      realdestdir = str(instdir / destdir.replace('/home/username/games/', ''))
      # print(f'Searching for dir matching [{destdir}]')
      basename = os.path.basename(destdir)
      parentdir = findDirInDir(basename, extdir)
      if parentdir:
        commands.append({'copy': str(Path(parentdir) / basename), 'to': realdestdir})
      else:
        if basename.upper() == 'MAIN':
          print('just using the first zip for the \'main\' folder')
          commands.append({'copy': str(extdir / mi.zipFilename[0]), 'to': realdestdir})
        else:
          print(f'couldn\'t find an extracted dir that matches the destination [{basename}], checking if this basename exists in one of the zip files')
          for zfn in mi.zipFilename:
            if basename.upper() in zfn.upper():
              # validate = True
              commands.append({'copy': str(extdir / zfn), 'to': realdestdir})
    if validate:
      for c in commands:
        print(f'{c[1]}\n-> {c[2]}')
      if 'N' == input('are these okay?').upper():
        commands = []

  return commands

def executeStrategy(cmds, confirm = False):
  for c in cmds:
    src = c[1]
    dst = c[2].replace('/home/username/games', 'c:\\Users\\Mike\\Desktop\\shared\\instal').replace('/', '\\')
    if confirm:
      input(f'Moving [{src}] to [{dst}]...')
    # sp.check_output(['move', '/Y', src, dst], shell=True)
    shutil.copytree(src, dst, dirs_exist_ok=('copyover' == c[0]))

def validate():
  errors = 0
  startndx=0
  for ndx, (mod, modinfo) in enumerate(loadProc().items()):
    if ndx < startndx:
      continue
    rmdir(extdir)
    print(f'\033[92m{ndx}: {mod}\033[0m')
    if len(modinfo.expectedFiles) > 1 or 'No' != modinfo.expectedFiles[0]:
      [extractDir(fn) for fn in modinfo.zipFilename]
      findExpectedFiles(modinfo)
    else:
      print('Nothing to search, skipping')
  print(f'errors {errors}')

def validateInstall(mi:ModInfo):
  filetypes = ['bsa', 'esp', 'esm']
  dirs = ['BookArt', 'Docs', 'Fonts', 'Icons', 'Meshes', 'Music', 'Sound', 'Splash', 'Textures', 'Video']
  #all dirs not in dirlist
  #all files not in filetyelist
  #expected files missing in root
  for installedpath in mi.extractPath:
    obj = os.scandir(path=installedpath)
  
    # List all files and directories in the specified path
    print("Files and Directories in '% s':" % path)
    for entry in obj:
        if entry.is_dir() or entry.is_file():
            print(entry.name)

def install(proc):
  startndx=0
  skip=[29, 80, 139, 213, 306, 316]
  for ndx, (mod, modinfo) in enumerate(proc.items()):
    if ndx < startndx or ndx in skip:
      continue
    rmdir(extdir)
    print(f'\033[92m{ndx}: Installing {mod}\033[0m')
    print(modinfo.zipFilename)
    [extractDir(fn) for fn in modinfo.zipFilename]
    cmds = []
    try:
      cmds = findStrategy(modinfo)
      for c in cmds:
        print(c)
      modinfo.installActions = cmds
    except BaseException as e:
      traceback.print_exc()
      print(e)
      input(f'no strategy for {mod}... press enter')
      
    # executeStrategy(cmds)

rmdir(instdir)
proc = loadProc()
install(proc)
saveProc(proc)
