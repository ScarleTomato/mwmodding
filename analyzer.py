from urllib.request import urlopen
from bs4 import BeautifulSoup
import csv
import os
import subprocess
from pathlib import Path
import oyaml as yml

# NEXUSMODS_KEY = os.environ['NEXUSMODS_KEY']

zipDir = Path(__file__).parent / 'zips'
NL = '\n'
COLOR_YELLOW = '\033[93m'
COLOR_GREEN = '\033[92m'
ENDC = '\033[0m'

class ModInfo():
    modUrl:str
    downloadUrl:str
    zipFilename:list = []
    extractPath:list
    expectedFiles:list
    installActions:list

    def toCsvRow(self):
        l = []
        for key, type in ModInfo.__dict__['__annotations__'].items():
            if key in self.__dict__ and type == str:
                l.append(self.__dict__[key])
            elif key in self.__dict__ and type == list:
                l.append(':'.join(self.__dict__[key]))
            else:
                l.append('')
        return l

    def fromCsvRow(l:list):
        ret = ModInfo()
        for idx, (key, type) in enumerate(ModInfo.__dict__['__annotations__'].items()):
            if list == type:
                ret.__dict__[key] = [] if not l[idx] else l[idx].split(':')
            else:
                ret.__dict__[key] = l[idx]
        return ret

def loadProc(filepath:str='proc.yml', type='yml'):
    proc = {}
    if type == 'yml':
        with open(filepath, newline='\n') as ymlfile:
          map = yml.safe_load(ymlfile)
          for k, v in map.items():
              mi = ModInfo()
              for j, w in v.items():
                  setattr(mi, j, w)
              proc[k] = mi
              
    else:
      if os.path.exists(filepath):
          with open(filepath, newline='\n') as csvfile:
              procreader = csv.reader(csvfile, delimiter=',', quotechar='|')
              for row in procreader:
                  modInfo = ModInfo.fromCsvRow(row)
                  proc[modInfo.modUrl]=modInfo

    return proc

def saveProc(proc:dict, filepath:str = 'proc.yml', type='yml'):
    if type == 'yml':
        map = {}
        for k, v in proc.items():
            map[k] = v.__dict__
        with open(filepath, 'w', newline="\n") as ymlfile:
            ymlfile.write(yml.dump(map))
    else:
      with open(filepath, 'w', newline="\n") as csvfile:
          writer = csv.writer(csvfile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
          for value in proc.values():
              writer.writerow(value.toCsvRow())

def getModPageList():
    mods = []
    with urlopen('https://modding-openmw.com/lists/total-overhaul/') as response:
        soup = BeautifulSoup(response, 'html.parser')
        for anchor in soup.find_all('div', {'class': "modname"}):
            mods.append(anchor.find_all('a')[1].get('href', '/'))

    print(f'Found {len(mods)} mods')
    return mods

def getModDownloadUrl(soup):
    return soup.find_all('i', {'class': ['fa-download']})[0].parent.get('href', '/')

def findFirstValByKey(soup, name:str):
    for key in soup.find_all('div', {'class': ['key']}):
        if name in key:
            return key.find_next_siblings('div')[0]

def getExtractPath(soup, os:str = 'Linux'):
    modDetailGrid = soup.find('p', {'id':['folder-paths']}).find_next_siblings('div')[0]
    for detail in modDetailGrid.find_all('div'):
        if detail.string is not None and os in detail.string:
            return detail.find_next_siblings('div')[0].code.string

def getModFiles(soup):
    return findFirstValByKey(soup, 'Has Plugin').code.string.strip().split('\n')

def toText(soup):
    return ' '.join(soup.findAll(string=True)).replace(':','').strip().replace('  ', ' ')

def parseModPage(url):
    with urlopen('https://modding-openmw.com' + url) as response:
        soup = BeautifulSoup(response, 'html.parser')
        d = {}
        for key in soup.find_all('div', {'class': ['key']}):
            d[toText(key)] = toText(key.find_next_siblings('div')[0])
        d['title'] = soup.title.string

        dlElem = soup.find('a', {'id': 'download'})
        dli = soup.find_all('i', {'class': ['fa-download']})
        if dlElem is not None:
            d['downloadUrl'] = 'https://modding-openmw.com' + dlElem.parent.find_next_siblings('div')[0].a.get('href', '/')
        elif len(dli) > 0:
            d['downloadUrl'] = dli[0].parent.get('href', '/')
        else:
            d['downloadUrl'] = 'https://modding-openmw.com' + url
    return d

def addUserZips(zips = []):
    for zip in zips:
        print(zip)
    zipfn = 'false'
    while zipfn:
        zipfn = input('\nWhat is the archive called?\n')
        # path does not exist (typed a filename that isn't in this dir)
        if not os.path.exists(zipDir / zipfn):
            print(f'\n{zipDir / zipfn} doesn\'t exist')
        # filename is not empty and file exists in this dir
        elif zipfn:
            zips.append(zipfn)
        elif len(zips) == 0:
            print('\nMust have at least one zip')
            zipfn = 'false'
    return zips
        

def procMod(modId, proc:dict):
    page = None

    ## Get relevant info from mod page
    if modId not in proc.keys():
        page = parseModPage(modId)
        mi = ModInfo()
        mi.modUrl = modId
        mi.downloadUrl = page['downloadUrl']
        mi.extractPath = page.get('Linux') or ''
        
        modInfo.expectedFiles = []
        for key in ['Has Plugins', 'Has Plugin', 'Requires BSA']:
            if key in page and page[key] != 'No':
              modInfo.expectedFiles.extend(page[key].split('\n'))

        proc[modId] = mi

    modInfo = proc[modId]

    ## Update expected Files
    modInfo.expectedFiles = []
    page = parseModPage(modId)
    for key in ['Has Plugins', 'Has Plugin', 'Requires BSA']:
        if key in page and page[key] != 'No':
          modInfo.expectedFiles.extend(page[key].split('\n'))
    print(modInfo.expectedFiles)


    ## get user to download zip
    if modInfo.extractPath and (not modInfo.zipFilename or len(modInfo.zipFilename) == 0):
        print(f'Visit {modInfo.downloadUrl} and save the archive to {zipDir.absolute()}')
        page = parseModPage(modId) if page is None else page
        if 'Usage Notes' in page:
            print(f'{COLOR_YELLOW}Usage Notes:\n{page["Usage Notes"]}{ENDC}')
        subprocess.call(['firefox', modInfo.downloadUrl])
        
        addUserZips(modInfo.zipFilename)

    ## install zip
    # print(f"Extracting {modInfo.zipFilename} to {modInfo.extractPath}")
    # print(f'Looking for files {modInfo.expectedFiles.split(NL)}')

def main():
  proc = loadProc()
  try:
      for n, mod in enumerate(getModPageList()):
          print(f'\n{n}: Processing mod {COLOR_GREEN}{mod}{ENDC}')
          procMod(mod, proc)
  finally:
      saveProc(proc)

if __name__ == '__main__':
    # main()
    # v=loadProc(filepath='proc.csv', type='csv')
    v=loadProc()
    print(v['/mods/patch-for-purists/'].__dict__)
    # saveProc(v)
